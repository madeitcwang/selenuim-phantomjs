# Scrapy抓取在不同级别Request之间传递参数
## http://www.jianshu.com/p/de61ed0f961d
>   def parse(self, response):
        # collect `item_urls`
        for item_url in item_urls:
            yield Request(url=item_url, callback=self.parse_item)

> def parse_item(self, response):
        item = MyItem()
        # populate `item` fields  收集处理一部分数据
        yield Request(url=item_details_url, meta={'item': item},
            callback=self.parse_details)

>  def parse_details(self, response):
        item = response.meta['item']
        # populate more `item` fields  再收集处理另外的数据
        return item
